const dotenv = require("dotenv");
dotenv.config();
const express = require("express");
const cors = require("cors");
const dbConnect = require("../config/dbConnect");
dbConnect();

const usersRoute = require("../routes/usersRoute")
const productsRoute = require("../routes/productsRoute")
const ordersRoute = require("../routes/ordersRoute")
const {errHandler,notFound} = require("../middlewares/errHandler")

const app = express();

//pass incoming data
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: true }));

//make use of created express routes
app.use("/users", usersRoute);
app.use("/products", productsRoute);
app.use("/orders", ordersRoute);
app.use(notFound);
app.use(errHandler);


module.exports = app;