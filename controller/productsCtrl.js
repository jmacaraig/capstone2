const Product = require("../model/Product");
const User = require("../model/User");
const asyncHandler = require("express-async-handler");
// const {handleCartOperation} = require("../middlewares/cartOperations.js");


/*
	
	MISSING FUNCTIONALITIES FOR FUTURE DEPLOYMENT:
		1. Upload Images for Product - Required
		2. Add Category, Colors, Brand - Required
		3. Add Filtering - Required
		4. Add Reviews - Required
		5. Add Discounts Coupons - Required
		6. Update Product Quantity once order is placed - Required
		7. Make Product not Active if quantity falls to 0 - Required
		
	ISSUES:
		1. Not active products should not appear on single fetch product for users
		2. If condition on archive and active is not working on render - Resolved
		3. Not throwing error on backend for same name or same product
		4. Create product function allows 0 stocks and isActive true
*/



//Not working as middleware on seperate file
const handleCartOperation = async (req, res, operation) => {

  const user = await User.findById(req.user.id);
  const product = await Product.findById(req.product);
  const itemExist = user.cart.find((item) => item.productId === product.id);

  if (operation === 'add' && req.body.quantity > product.availableQty) {
    throw new Error('Quantity being added to the cart is more than the available quantity.');
  }

  if (operation === 'increase' || operation === 'decrease') {
    if (!itemExist) {
      throw new Error("Item is not yet added to the cart.");
    }
  }

  if (operation === 'add' || operation === 'increase') {
    const quantity = operation === 'add' ? (req.body.quantity || 1) : 1;
    const amount = quantity * product.price;

    if (!itemExist) {
      const productFields = {
        productId: product.id,
        name: product.name,
        description: product.description,
        quantity: quantity,
        subTotal: amount,
      };

      user.cart.push(productFields);
    } else {
      itemExist.quantity += quantity;
      itemExist.subTotal = itemExist.quantity * product.price;
    }
  } else if (operation === 'decrease') {
    itemExist.quantity -= 1;
    itemExist.subTotal = itemExist.quantity * product.price;

    if (itemExist.quantity <= 0) {
      const index = user.cart.findIndex((item) => item.productId === product.id);
      if (index !== -1) {
        user.cart.splice(index, 1);
      }
    }
 
  }

  user.cartTotal = user.cart.reduce((total, item) => total + item.subTotal, 0);

  user.markModified('cart');
  await user.save();

  return 'Cart Updated.';
};


//Create a product
module.exports.createProduct = asyncHandler(async(req,res)=>{

	const {name, brand, type, description, price, stocks, images, user} = req.body; 

	const productExist = await Product.findOne({name});


	if (productExist){

		throw new Error(`Product Already Exists`);

	}

	const product = await Product.create({
		name, 
		brand,
		type,
		description,
		price,
		stocks,
		images,
		sellerId: user
	});


	res.json({
		status: "Success",
		message: "Product created successfully",
		product
	})

});


//Get All Products (Admin Only)
module.exports.getAllProducts = asyncHandler(async (req, res) => {
  // console.log("Route OK"); //Route-check

  const products = await Product.find({});

  for (const product of products) {
    if (product.stocks <= 0) {
      product.isActive = false;
      await product.save()
    } 

  }
  await res.json(products);

});

module.exports.getActiveProducts = asyncHandler(async (req, res) =>{
	// console.log("Route OK");

	//res.send("Route OK"); //Route-check

	const products = await Product.find({isActive : true});

	if (products.length === 0){

		res.json({
		status: "Success",
		message: "No products available yet."
	});

	} else {

	res.json(products)

	}


});


module.exports.getSingleProduct = asyncHandler(async (req, res) =>{

	//res.send("Route OK"); //Route-check

	const showProduct = await Product.findById(req.product);


	res.json(showProduct);

	});


module.exports.updateProduct = asyncHandler(async (req, res) =>{

	const updatedFields = {
		name: req.body.name, 
		description: req.body.description, 
		price: req.body.price,
		stocks: req.body.stocks, 
		updatedOn: new Date()
	}


	if (updatedFields.name === "" || updatedFields.description === "" || updatedFields.price==="" || updatedFields.stocks === ""){


	throw new Error("Please input any of the following to update a product: name, description or price.");		

	}

	const product = await Product.findByIdAndUpdate(req.product, updatedFields, {new: true});

		res.json({
		status: "Success",
		message: "Product Updated Successfully.",
		product

	});

	

});


module.exports.archiveProduct = asyncHandler(async (req,res)=>{ 

	//res.send("Route OK"); //Route-check

	// console.log(req.product.isActive) //Render Checking

	if (!req.product.isActive){

		throw new Error("Product is already on archive status.");		

	}

	const archive = await Product.findByIdAndUpdate(req.product, {isActive: false, updatedOn: new Date()}, {new: true});

	res.json({
		status: "Success",
		message: "Product Archived Successfully.",
		archive
	});
	

});


module.exports.activateProduct = asyncHandler(async (req,res)=>{

	//res.send("Route OK"); //Route-check

	// console.log(req.product.isActive) //Render Checking

	if (req.product.isActive){

		throw new Error("Product is already on active status.");		

	}

	const activate = await Product.findByIdAndUpdate(req.product, {isActive: true, updatedOn: new Date()}, {new: true})

	res.json({
		status: "Success",
		message: "Product Activated Successfully.",
		activate
	});

});

module.exports.removeCartItem = asyncHandler(async (req,res) => {

	const user = await User.findById(req.user.id);
  const { activeItem } = req.body;

  if (activeItem.length !== 0){
			for (const item of activeItem) {
		      user.cart = user.cart.filter(cartItem => cartItem.productId !== item.productId);
		    }

		  await user.save();

		   res.json({
		      message: "Item removed."
		    });

		} else {

				res.json({
      		message: "Please select product(s) to remove."
    });

		}

})


module.exports.addToCart = asyncHandler(async (req, res) => {

  const message = await handleCartOperation(req, res, 'add');
  res.json({
    message: message,
  });
});


module.exports.increaseItemOnCart = asyncHandler(async (req, res) => {
  const message = await handleCartOperation(req, res, 'increase');
  res.json({
    message: message,
  });
});


module.exports.decreaseItemOnCart = asyncHandler(async (req, res) => {
  const message = await handleCartOperation(req, res, 'decrease');
  res.json({
    message: message,
  });
});