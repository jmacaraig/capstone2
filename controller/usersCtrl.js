const User = require("../model/User");
const bcrypt = require("bcrypt");
const asyncHandler = require("express-async-handler");
const generateToken = require("../utils/generateToken");


/*
	
	MISSING FUNCTIONALITIES FOR FUTURE DEPLOYMENT:
		1. Change Password - Required
		2. Add Contact Number - Required
		3. Add Verification Email / Mobile - Required
		4. Add Address on Registration (Can be assigned as Shipping Address) - Required
		5. Update Name? - TBC
		6. Delete User (Old Admins) - TBC if needed.
		7. Add Wallet? - TBC

	ISSUES:
		1. Registration - Checking for empty password
		2. Login - Checking for empty password
		3. Error [ERR_HTTP_HEADERS_SENT] - Resolved
		
		 

*/


//desc: Register User //route: POST /users/register

module.exports.registerUser = asyncHandler(async (req,res) => {

	console.log("Route OK"); //Route-check 

	const {firstName, lastName, email, password} = req.body

	const userExists = await User.findOne({email});

	if (userExists){

		throw new Error(`User email ${email} already exist. Please use a different email.`)

	}

	const salt = await bcrypt.genSalt(10);
	const hashedPassword = await bcrypt.hash(password,salt);

//If email is not used, create user
	const user = await User.create({
		firstName, lastName, email, password: hashedPassword
	});

	res.json({
		status: "Success",
		msg: "User Registered Successfully",
		data: user
	});

});


//desc: Login User //route: POST /users/login

module.exports.loginUser = asyncHandler(async (req, res) => {

	// console.log("Route OK"); //Route-check 

	const {email, password} = req.body

	// console.log({email, password})

	const userRegistered = await User.findOne({email});

	if (!userRegistered){

		res.json({msg: `Cannot find user with email ${email}`});		

	}

	if (userRegistered && await bcrypt.compare(password, userRegistered?.password)){

		userRegistered.lastLogin = new Date();
    	await userRegistered.save();

		return res.json({
			status: "Success",
			msg: `Logged in Successfully!`,
			token: generateToken.createToken(userRegistered),
			userRegistered
			});

	} else {

		throw new Error("Invalid login credentials.")

}

});


//desc: Retrieve User Details (Not Admin) //route: GET /users/userDetails //Token: Required

module.exports.userDetails = async (req,res) => {

	//res.send("Route OK"); //Route-check 

	const user = await User.findById(req.user.id)

	res.json(user)

}

//desc: Retrieve User Cart (Not Admin) //route: GET /users/:id/userCart //Token: Required

module.exports.userCart = async (req,res) => {

	//res.send("Route OK"); //Route-check 

	const user = await User.findById(req.user.id)

	res.json(user.cart)

}



//desc: Retrieve All User Details (Admin Only) //route: GET /users/allUsers //Token: Required

module.exports.allUsers = asyncHandler(async (req,res) => {

	//res.send("Route OK"); //Route-check

	//Add Checking by ID??

	const users = await User.find({})

	res.json(users)

});


//desc: Update User as Admin (Admin Only) //route: PUT /users/updateAdmin //Token: Required

module.exports.updateAdmin = asyncHandler(async (req, res) => {
    
    const user = await User.findById(req.body.changeId);

     if (!user) {
        
        throw new Error('User not found.');

     }

     if (!req.user.isAdmin) {
        
        throw new Error("You don't have permission to perform this action." );
      
     }

     if (user.isAdmin){

      	return res.json({message: 'User is already and admin.'});

     } else {

     const updatedUser = await User.findByIdAndUpdate(
        user,
        { isAdmin: true },
        { new: true }
     );

      return res.json({ message: 'User has been updated as an admin.', updatedUser });

  	}	


    
 });


//desc: Remove User as Admin (Admin Only) //route: PUT /users/removeAdmin //Token: Required

module.exports.removeAdmin = asyncHandler(async (req, res) => {
    
    const user = await User.findById(req.body.changeId);

     if (!user) {
        
        throw new Error('User not found.');

     }

     if (!req.user.isAdmin) {
        
        throw new Error("You don't have permission to perform this action." );
      
     }

     if (!user.isAdmin){

      	return res.json({message: 'User is already not an admin.'});

     } else {

     const updatedUser = await User.findByIdAndUpdate(
        user,
        { isAdmin: false },
        { new: true }
     );

      return res.json({ message: 'User has been removed as an admin.', updatedUser });

  	}	


    
 });