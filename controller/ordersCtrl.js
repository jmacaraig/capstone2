const Order = require("../model/Order");
const Product = require("../model/Product");
const User = require("../model/User");
const asyncHandler = require("express-async-handler"); 


/*
	
	MISSING FUNCTIONALITIES FOR FUTURE DEPLOYMENT:
		1. Add Payment Method - Required
		2. Add Payment Webhook? - Required
		3. Add Paymet Status - Required
		4. Add Order Status - Required
		5. Add Order Cancellation - Required
		6. Add Refund - Required
		

*/



module.exports.createItemOrder = asyncHandler(async (req,res) => {

	// res.send("Route OK"); //Route-check

	const user = await User.findById(req.user.id);
	const product = await Product.findById(req.params.id);

	if (req.body.quantity > product.availableQty){

		throw new Error("Quantity being ordered is more than the available quantity.");

	}

    const quantity = req.body.quantity || 1;
    const amount = quantity * product.price;

	const order = await Order.create({
		userId: user,
		totalAmount: amount,
		quantity: quantity,
		products: [product]
	})

	if (order){

		const productFields = await {
			name: product.name,
			description: product.description,
			quantity: order.quantity,
			totalPrice: order.totalAmount,
			purchasedOn: new Date
	}

		user.orders.push(productFields);
		await user.save()

		res.json({
		message: "Order has been created."
	});

	} else {

		throw new Error("A problem occured during order creation.");

	}

	
});


module.exports.createCartOrder = asyncHandler(async (req, res) => {
  
  const user = await User.findById(req.user.id);
  const { activeOrder, total } = req.body;
  const productsWithSellerId = [];


 if (activeOrder.length !== 0){
  for (const item of activeOrder) {
    const product = await Product.findById(item.productId);

    if (!product) {
      res.status(400).json({ message: `Product not found for productId: ${item.productId}` });
      return;
    }

    if (item.quantity <= product.stocks){

    console.log(item.quantity)
    console.log(product.stocks)
   
    const productWithQuantity = {
        ...product.toObject(),
        quantityOrdered: item.quantity,
      };

    productsWithSellerId.push(productWithQuantity);

    product.stocks -= item.quantity

    await product.save()

    } else {
    	res.status(400).json({ message: `Ordered quantity is more than stock(s)` });
      return;
    }
  }

  if(productsWithSellerId.length !==0){
  const order = await Order.create({
    userId: user,
    totalAmount: total,
    products: productsWithSellerId,
  });


  if (order) {

  	order.orderId = order._id;

  	user.orders.push(order);

    for (const item of activeOrder) {
      user.cart = user.cart.filter(cartItem => cartItem.productId !== item.productId);
    }

    await user.save();

    res.json({
      message: "Order has been created."
    });
  } else {
    res.json({
      message: "Failed."
    });
  }

}else {
	    res.json({
      message: "Failed."
    });
}

} else {
	  res.json({
      message: "No product(s) selected."
    });
}	
});



module.exports.ordersList = asyncHandler (async (req,res) => {

	// res.send("Route OK"); //Route-check 

	const user = await User.findById(req.user.id)

	if (user.orders.length === 0){

		res.json({
			status: "Success",
			message: "You haven't made any order(s) yet."
		})

	} else {

	res.json(user.orders);

	}

});


module.exports.allOrders = asyncHandler(async (req,res) => {

	// res.send("Route OK"); //Route-check 

	//Add checking by ID??
	const userId = req.user.id;
	const orders = await Order.find({})

	let adminProduct = []

  await orders.forEach((order) => {
    let orderAlreadyAdded = false;

    order.products.forEach((product) => {
      if (product.sellerId.equals(userId) && !orderAlreadyAdded) {
        adminProduct.push(order);
        orderAlreadyAdded = true;
      }
    });
  });

	if (adminProduct.length === 0){

		res.json({
			status: "Success",
			message: "No order(s) has been made yet."
		})

	} else {

	res.json(adminProduct)

	}

});


