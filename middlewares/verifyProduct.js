const Product = require("../model/Product");

const asyncHandler = require("express-async-handler");

//Product deleted or removed unexpectedly
module.exports.checkProductAvailability = asyncHandler(async (req, res, next) => {

    const product = await Product.findById(req.params.id);

    if (!product) {
      throw new Error("Item or Product no longer exists.");
    }

    req.product = product; 
    next();

  
});