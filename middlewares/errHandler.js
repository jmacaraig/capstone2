

module.exports.errHandler = (err, req, res, next) => {
	
	if (res.headersSent) {
    return res.end();
  	}

	const statusCode = 500;
	const message = err?.message;

	res.status(statusCode).json({message});

}


//404 Handler
module.exports.notFound = (req, res, next) => {

	const err = new Error(`Sorry. This link is not available.`);
	next(err);

}