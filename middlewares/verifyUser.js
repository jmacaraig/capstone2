const jwt = require("jsonwebtoken");
const secret = "Capstone2-Ecommerce-API"; //can be placed on .env

const asyncHandler = require("express-async-handler");

module.exports.isRegistered = asyncHandler((req,res,next)=>{

	let token = req.headers.authorization

	if(typeof token === "undefined"){
		return res.json({
			status: "Failed.",
			message:"Please register or login to continue."
	});

	} else {

		token = token.slice(7,token.length);

		jwt.verify(token, secret, function(err,decodedToken){

		if(err){

		return res.json({
			status: "Failed.",
			message:"Invalid or expired token. Please login again to continue."
		});
		
		} else {
				req.user = decodedToken
				next()
		}
		})
	}
});


module.exports.isUserAdmin = asyncHandler((req,res,next)=>{

	if(req.user.isAdmin){
		next()

	} else {
		
		return res.json({
			status:"Failed.",
			message:"Action Forbidden. Requires Admin Access."
		})
	}
});


module.exports.stopAdmin = asyncHandler((req,res,next)=>{

	if(!req.user.isAdmin){
		next()

	} else {
		
		return res.json({
			status:"Failed.",
			message:"Admin is not allowed to this action."
		})
	}
});