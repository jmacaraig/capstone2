const express = require("express"); 

const productsCtrl = require("../controller/productsCtrl")

const {isRegistered, isUserAdmin, stopAdmin} = require("../middlewares/verifyUser");

const {checkProductAvailability} = require("../middlewares/verifyProduct");

const productsRoute = express.Router();


productsRoute.post("/createProduct", isRegistered, isUserAdmin, productsCtrl.createProduct); //Working on localhost (Routes and Controller)

productsRoute.get("/getAllProducts", isRegistered, isUserAdmin, productsCtrl.getAllProducts);  //Working on localhost (Routes and Controller)

productsRoute.get("/getActiveProducts", productsCtrl.getActiveProducts); //Working on localhost (Routes and Controller)

productsRoute.get("/:id/getSingleProduct", checkProductAvailability, productsCtrl.getSingleProduct); //Working on localhost (Routes and Controller)

productsRoute.put("/:id/updateProduct", isRegistered, isUserAdmin, checkProductAvailability, productsCtrl.updateProduct); //Working on localhost (Routes and Controller)

productsRoute.put("/:id/archiveProduct", isRegistered, isUserAdmin, checkProductAvailability, productsCtrl.archiveProduct); //Working on localhost (Routes and Controller)

productsRoute.put("/:id/activateProduct", isRegistered, isUserAdmin, checkProductAvailability, productsCtrl.activateProduct); //Working on localhost (Routes and Controller)

productsRoute.post("/:id/addToCart", isRegistered, stopAdmin, checkProductAvailability, productsCtrl.addToCart); //Working on localhost (Routes and Controller)

productsRoute.put("/removeCartItem", isRegistered, stopAdmin, productsCtrl.removeCartItem);

productsRoute.put("/:id/increaseItemOnCart", isRegistered, stopAdmin, checkProductAvailability, productsCtrl.increaseItemOnCart); //Working on localhost (Routes and Controller)

productsRoute.put("/:id/decreaseItemOnCart", isRegistered, stopAdmin, checkProductAvailability, productsCtrl.decreaseItemOnCart); //Working on localhost (Routes and Controller)


module.exports = productsRoute;