const express = require("express");

const usersCtrl = require("../controller/usersCtrl")

const {isRegistered, isUserAdmin} = require("../middlewares/verifyUser");

const usersRoute = express.Router();


usersRoute.post('/register', usersCtrl.registerUser); //Working on localhost (Routes and Controller)

usersRoute.post('/login', usersCtrl.loginUser); //Working on localhost (Routes and Controller)

usersRoute.get('/userDetails', isRegistered, usersCtrl.userDetails); //Working on localhost (Routes and Controller)

usersRoute.get('/:id/userCart', isRegistered, usersCtrl.userCart); //Working on localhost (Routes and Controller)

usersRoute.get('/allUsers', isRegistered, isUserAdmin, usersCtrl.allUsers); //Working on localhost (Routes and Controller)

usersRoute.put('/updateAdmin', isRegistered, isUserAdmin, usersCtrl.updateAdmin); //Working on localhost (Routes and Controller)

usersRoute.put('/removeAdmin', isRegistered, isUserAdmin, usersCtrl.removeAdmin); //Working on localhost (Routes and Controller) 


module.exports = usersRoute;