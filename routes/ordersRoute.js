const express = require("express");

const ordersCtrl = require("../controller/ordersCtrl")

const {isRegistered, isUserAdmin, stopAdmin} = require("../middlewares/verifyUser");

const {checkProductAvailability} = require("../middlewares/verifyProduct");

const ordersRoute = express.Router();

ordersRoute.post("/:id/createItemOrder", isRegistered, stopAdmin, checkProductAvailability, ordersCtrl.createItemOrder); //Working on localhost (Routes and Controller)

ordersRoute.post("/createCartOrder", isRegistered, stopAdmin, ordersCtrl.createCartOrder); //Working on localhost (Routes and Controller)

//Transfer to UsersRoute?
ordersRoute.get("/ordersList", isRegistered, stopAdmin, ordersCtrl.ordersList); //Working on localhost (Routes and Controller)

//Transfer to UsersRoute?
ordersRoute.get("/allOrders", isRegistered, isUserAdmin, ordersCtrl.allOrders); //Working on localhost (Routes and Controller)




module.exports = ordersRoute;