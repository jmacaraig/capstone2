const jwt = require("jsonwebtoken");
const secret = "Capstone2-Ecommerce-API";

module.exports.createToken = (user)=>{
	
	const data = {
		id: user._id,
		email:user.email,
		isAdmin: user.isAdmin
	};
	return jwt.sign(data, secret, {expiresIn:"2d"});
}


