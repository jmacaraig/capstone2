const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: true
	},
	lastName: {
		type: String,
		required: true
	},
	email: {
		type: String,
		required: true
	},	
	password: {
		type: String,
		required: true
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	lastLogin: {
		type: Date
	},
	cart: [{
		type: Object, //mongoose.Schema.Types.ObjectId (if ID only),
		ref: "Product"
	}],
	cartTotal: {
		type: Number,
		default: 0,
		ref: "Product"
	},
	orders: [{

		type: Object, //mongoose.Schema.Types.ObjectId (if ID only),
		ref: "Order"
	}]
	},
	{
	timeStamps: true,
})

module.exports = mongoose.model("User",userSchema)