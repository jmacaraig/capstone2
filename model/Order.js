const mongoose = require("mongoose"); 

const orderSchema = new mongoose.Schema({

	userId: {
		type: mongoose.Schema.Types.ObjectId,
		ref: "User",
		required: true
	},
	products: [
	{
		type: Object,
		required: true
	}
	],
	quantity: {
		type: Number,
		required: false
	},
	totalAmount: {
		type: Number,
		required: true
	},	
	purchasedOn: {
		type: Date,
		default: new Date()
	}
	},
	{
	timeStamps: true,
})

module.exports = mongoose.model("Order",orderSchema);


/*
paymentStatus: {
	type: "String",
	required: true,
	default: "Waiting for Payment."
},
paymentMethod: {
	type: "String",
	default: "Add Payment Method."
},
orderStatus: {
	type: "String",
	default: "Pending.",
	enum: ["Pending", "Processing", "Shipped", "Delivered"]
},
orderNumber:{
	type: String,
	required: true,
	default: randomNumbers
},
shippingAddress:{
	type: Object,
	required: true
}
*/