const mongoose = require("mongoose"); 

const productSchema = new mongoose.Schema({

	name: {
		type: String,
		required: true
	},
	brand: {
		type: String,
		required: false
	},
	type: {
		type: String,
		required: false
	},
	description: {
		type: String,
		required: true
	},
	price: {
		type: Number,
		required: true
	},
	stocks: {
		type: Number,
		required: true
	},
	images: {
		type: Array,
		required: false
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},

//Added propery to know product's last update time
	updatedOn: {
		type: Date,
		required: false
	},
	sellerId: {
		type: mongoose.Schema.Types.ObjectId,
		required: true,
		ref: "User"
	}

	},
	{
	timeStamps: true,
})

module.exports = mongoose.model("Product",productSchema);