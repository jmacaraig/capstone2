const mongoose = require("mongoose");

const dbConnect = async () => {

try {

	const connected = await mongoose.connect(process.env.MONGO_URL,{

	useNewUrlParser: true,
	useUnifiedTopology: true

}); console.log("Now Connected to MongoDB Atlas.")

} catch (error) { console.error.bind(console, "Connection error.");

	process.exit(1);
	
};

}

module.exports = mongoose;
module.exports = dbConnect;