const http = require("http");

const app = require("./app/app");

const server = http.createServer(app);

const port = 4000;

app.listen(process.env.PORT || port, ()=>{
		console.log(`API is now online on port ${process.env.PORT || port}`)})